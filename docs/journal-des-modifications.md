# Journal des modifications

## Version 2024

### Finale
02/07/2024 - Publication de la version Juillet 2024 après prise en compte des retours de la consultation publique.

### Appel à commentaires
du 25 mars 2024 au 3 juin 2024

Le glossaire est séparé du document doctrine et devient un document à part entière.

## Version 1 (2023)

### Version 1.1 Web
01/01/2024 - publication de la version 1.1 dans une version web

### Version 1.1
27/06/2023 - Correction de la numérotation dans la *Partie 6 - Règles et cadres de référence*

### Version 1.0
23/05/2023 - Publication d'une version 1 du référentiel après prise en compte des retours de la consultation publique