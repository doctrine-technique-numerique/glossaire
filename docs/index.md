# 

Ce document est le [**glossaire**](https://doctrine-technique-numerique.forge.apps.education.fr/glossaire/texte/glossaire/) du corpus documentaire de la [**doctrine technique du numérique pour l’éducation**](https://doctrine-technique-numerique.forge.apps.education.fr/).

Ce corpus documentaire comprend :

* La [doctrine technique du numérique pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/)
* Le [cadre général de sécurité des services numériques pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/securite/)
* Le [référentiel d'interopérabilité des services numériques pour l'éducaiton](https://doctrine-technique-numerique.forge.apps.education.fr/interoperabilite/)
* Le [référentiel du numérique responsable pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/numerique-responsable/)
* Le [glossaire](https://doctrine-technique-numerique.forge.apps.education.fr/glossaire/texte/glossaire/)

Ce glossaire intègre aussi des termes du [schéma directeur des ENT (SDET) 2024](https://eduscol.education.fr/1559/schema-directeur-des-ent-sdet-version-en-vigueur).