# 

## A

### Accord de fédération

Le domaine de confiance de la fédération est régi par **un ou des accords de fédération**, mis en place par les porteurs de projet, en lien avec les différentes parties prenantes de la fédération. C’est cet accord qui définit les droits et devoirs des membres de la fédération.

Il contient les règles suivantes :

* identification des intervenants dans la fédération d’identités ;
* périmètre de fédération ;
* engagements du fournisseur d’identité ;
* identité et attributs produits ;
* engagements du fournisseur de service ;
* engagements réciproques ;
* durée de l’accord, principes de renouvellement et de rupture ;
* coûts.

Dans le cas d’une relation multiple (plusieurs fournisseurs de service / fournisseurs d’identité), l’accord peut être :

* global (mais validé et signé par chaque partie concernée) si le cycle de vie de la relation de fédération est identique et que les conditions et procédures de fédération sont identiques ; on retrouvera cette configuration dans le cas des relations intra Éducation nationale ;
* au cas par cas ; on pourra retrouver ce cas dans les relations entre l’Éducation nationale et un partenaire.

Les engagements suivants sont pris par **les fournisseurs d’identité et les fournisseurs de service** :

* respect de l’objet et des règles communes de fonctionnement de la fédération ;
* gestion des identités et des autorisations selon des procédures formalisées et diffusées ;
* protection des données à caractère personnel ;
* respect de règles de sécurité ;
* utilisation des standards technologiques définis.

Les engagements suivants sont pris par **les fournisseurs d’identité** :

* gestion des moyens d’authentification ;
* obligation réglementaire de traçabilité, définition, mise à jour et respect des données partagées ;
* définition le cas échéant d’une notion d’identifiant unique sur le périmètre de la fédération et de sa forme ;
    * journalisation des usages du service d’identification / authentification.
* La gouvernance de la fédération est assurée, notamment afin de :
    * définir l’organisation de la fédération ;
    * définir et faire vivre l’objet et les règles communes de fonctionnement de la fédération.
* La fédération est administrée, notamment afin de :
    * définir le statut administratif ;
    * définir et distribuer les données partagées par tous les membres ;
    * définir les orientations technologiques supportées (standards utilisés) et les règles de sécurité à suivre ;
    * traiter les demandes d’inscription et de départ ;
    * contrôler les engagements des membres de la fédération ;
    * appliquer les évolutions des règles de fonctionnement.
* Les standards technologiques de la fédération sont définis.

### ACPM

L’alliance pour les chiffres de la presse et des médias est une association professionnelle française, dont la mission est d'être le tiers certificateur des médias. L'ACPM contrôle, certifie et donne de la valeur aux chiffres des médias. L'ACPM est issue de la fusion entre Audiopresse et l'OJD (office de justification de la diffusion). L'OJD, association indépendante, est un organisme interprofessionnel de contrôle de la diffusion de la presse avec une activité, depuis 20 ans, de certification de l'audience internet.

### ADEME

L’ADEME est un Établissement public à caractère industriel et commercial (EPIC) placé sous la tutelle des ministères de la Transition écologique et de la Cohésion des territoires, de la Transition énergétique et de l’Enseignement supérieur et de la Recherche.

### AGEC

Loi du 10 février 2020 relative à la lutte contre le gaspillage et a l’économie circulaire entend accélérer le changement de modèle de production et de consommation afin de limiter les déchets et préserver les ressources naturelles, la biodiversité et le climat.

### ANSSI

Agence nationale de la sécurité des systèmes d’information.

L’[ANSSI]([http://www.ssi.gouv.fr/](http://www.ssi.gouv.fr/)) a mission d’autorité nationale en matière de sécurité et de défense des systèmes d’information. Pour ce faire, elle déploie un large panel d’actions normatives et pratiques, depuis l’émission de règles et la vérification de leur application, jusqu’à la veille, l’alerte et la réaction rapide face aux cyberattaques — notamment sur les réseaux de l’État.

### API

En informatique, une interface de programmation d’application ou interface de programmation applicative (*Application Programming Interface*) est un ensemble normalisé de classes, de méthodes, de fonctions et de constantes qui sert de façade par laquelle un logiciel offre des services à d'autres logiciels. Elle est offerte par une bibliothèque logicielle ou un service web, le plus souvent accompagnée d'une description qui spécifie comment des programmes « consommateurs » peuvent se servir des fonctionnalités du programme « fournisseur ».[^30]

[^30]: <https://fr.wikipedia.org/wiki/Interface_de_programmation>

### ARCEP

L’Arcep (Autorité de régulation des communications électroniques, des postes et de la distribution de la presse) est une autorité administrative indépendante. Elle assure la régulation des secteurs des communications électroniques et des postes, au nom de l’État, mais en toute indépendance par rapport au pouvoir politique et aux acteurs économiques.

### Authentification/identification

Un service d’identification/authentification assure l’authentification des utilisateurs à partir de la réception et de la vérification d’un couple « identifiant / authentifiant ». Il permet également la gestion du cycle de vie des identités et des authentifiants.

L’identification permet de connaître l’identité d’une entité, alors que l’authentification permet de vérifier l’identité.

Il existe plusieurs facteurs d’authentification : utiliser une information que seul le prétendant connaît (mot de passe), possède (carte à puce), est (données biométriques), peut produire (un geste).

Les protocoles d’authentification décrivent les interactions entre un prouveur et un vérifieur et les messages permettant de prouver l’identité d’une entité. On distingue deux familles de protocoles d’authentification : l’authentification simple (un seul facteur d’authentification en jeu) et l’authentification forte (deux facteurs ou plus). Par ailleurs, on parle d’authentification unique lorsqu’un utilisateur n’a besoin de procéder qu’à une seule authentification pour accéder à plusieurs applications informatiques.

### Autorisation d’accès

Les autorisations définissent quels utilisateurs (caractérisés par un identifiant et un ou plusieurs attributs) peuvent effectuer des actions sur des ressources, éventuellement sous certaines conditions.

Une action sur une ressource définit une habilitation.

Une action peut être une opération de lecture, écriture, modification ou suppression.

Une ressource peut être un service applicatif, une partie de service, une application, une page Web…

Une condition peut être une restriction d’accès au service applicatif, par exemple en fonction de l’horaire ou de la typologie d’accès.

Le service d’autorisation permet de contrôler les autorisations, c’est-à-dire à la fois de vérifier l’existence d’une association entre un utilisateur et une habilitation mais également que les conditions éventuelles sont satisfaites.

Le service d’autorisation permet également la gestion du cycle de vie des autorisations.

## B

### Backend

En informatique, backend (parfois aussi appelé un dorsal) est un terme désignant un étage de sortie d'un logiciel devant produire un résultat. On l'oppose au frontend (aussi appelé un frontal) qui lui est la partie visible de l'iceberg.

### BYOD (Bring Your Own Device)

Le BYOD est une politique qui permet aux membres d’une organisation d’apporter et d’utiliser leurs propres terminaux afin d’accéder aux ressources et applications internes de l’entreprise.

Par extension le BYOD est également utilisé dans le cas où un établissement scolaire laisse ses élèves utiliser les ressources de l’établissement avec les appareils personnels des élèves.

L’acronyme français correspondant est AVAN (Avec Votre Appareil Numérique) ou AVEC (Apportez Votre Equipement personnel de Communication).

## C

### Cahier de textes

Le cahier de textes a pour vocation d'apporter une aide au service des activités d'enseignement et d'apprentissage, en même temps qu'une facilité d'accès accrue pour tous les utilisateurs : les enseignants et l'équipe éducative dans son ensemble, les élèves mais aussi leurs parents (ou représentants légaux). Le cahier de textes numérique s’est substitué aux cahiers de textes sous forme papier depuis la rentrée 2011 (BO n°32 du 09/09/2010). Il constitue un document officiel, à valeur juridique. Le cahier de textes de classe sert de référence aux cahiers de textes individuels. De façon permanente, il doit être à la disposition des élèves et de leurs représentants légaux qui peuvent s'y reporter à tout moment. Il assure la liaison entre les différents utilisateurs. Il permet, en cas d'absence ou de mutation d'un professeur, de ménager une étroite continuité entre l'enseignement du professeur et celui de son suppléant ou de son successeur.

[Bulletin officiel n°32 du 9 septembre 2010](https://www.education.gouv.fr/bo/2010/32/mene1020076c.htm)

### Communs numériques

Le terme « communs numériques » désigne un ensemble de ressources numériques produites et gérées par une communauté. Par nature, ils sont partagés et collectifs.

Cette offre de l’État sera constituée à la fois d’outils souverains, sécurisés, libres et communautaires.

### Contrat d’interface

Un contrat d’interface désigne le contrat qui définit les modalités de l’accord entre deux parties à propos d’une interface. Il détermine clairement les données qui seront accessibles par les parties et sous quelles règles.

Dans le cas d’une API, il peut décrire notamment :

* le fonctionnement de l’API
* ses modalités d’accès en environnements d’intégration et de production,
* les champs et documents échangés et leur format.

### Convention de service dans le cadre des accords de fédération

La convention de service comporte la liste des parties concernées, par exemple :

* le responsable de traitement de l’ENT (chef d’établissement, IA-DASEN) dans le cas de transmission de données à caractère personnel ;
* le(s) responsable(s) de la mise en œuvre du projet ENT (collectivités, services académiques) ;
* le responsable du service Tiers.
* La convention de service comporte le rôle de chacun :
    * responsable de traitement ;
    * fournisseur d’identité ;
    * fournisseur de service ;
* Concernant l’interopérabilité de la solution ENT avec des services Tiers, une convention de service est élaborée afin que les rôles respectifs, les engagements et les modalités de traitement des données à caractère personnel des acteurs soient précisément définis.

L’organisation entre les différentes parties est précisée dans la convention de service. En particulier, elle fera apparaître : 

* les moyens mis en œuvre pour assurer la coordination entre les différentes parties ; 
* les conditions d’adhésion et de retrait du fournisseur d’identité ;
* les conditions d’adhésion et de retrait du responsable du service Tiers ;
* les relations entre membres : définition des relations bilatérales acceptées entre un fournisseur de service et un fournisseur d’identité ;
* la définition des documents de référence (dont les documents d’architecture technique).

La convention de service précise les engagements des différents acteurs, à savoir :

* les responsabilités communes ;
* les engagements des fournisseurs d’identité ;
* les engagements des fournisseurs de service ;
* les engagements des administrateurs de la solution ENT ;
* la durée de l’accord et les conditions de rupture et de renouvellement.

La convention de service fait apparaître notamment les conditions (qualité de service par exemple) et modalités d’accès et de retrait d’un utilisateur à un service Tiers, notamment, celle-ci mentionnera les éléments suivants :

* accès / retrait d’un utilisateur de sa propre initiative (via l’ENT, via un formulaire en ligne à la première connexion…) ;
* accès / retrait d’un utilisateur par un tiers (directeur d’école, chef d’établissement, administrateur de l’ENT…) ;
* durée de conservation, récupération et suppression des données à caractère personnel dont les données produites dans les services Tiers.

La convention de service stipule clairement les données nécessaires devant être transmises par l’ENT afin d’assurer l’authentification et le contrôle d’accès et indispensables au fonctionnement du service Tiers ainsi que les destinataires et la finalité du traitement de données à caractère personnel le cas échéant, en respectant les données autorisées selon la catégorie du services Tiers définies plus haut et les dispositions légales et réglementaires en vigueur. 

La convention de service précise la capacité d’un des acteurs du projet à sous-traiter tout ou partie de ses activités. Le cas échéant, les conditions, devoirs et responsabilités relatifs à cette sous-traitance sont précisés dans la convention. 

## D

### DINUM
La direction interministérielle du numérique (DINUM) est en charge de la transformation numérique de l'État au bénéfice du citoyen comme de l'agent, sous tous ses aspects : modernisation du système d'information de l'État, qualité des services publics numériques, création de services innovants pour les citoyens…

### DNMA

Le dispositif national de mesure d’audience (DNMA) des ENT est un dispositif de suivi de fréquentation reposant sur une solution de marquage externe. Celle-ci se conforme à un cahier des charges partagé qui définit et structure le contenu des marqueurs. Le DNMA rend comparables les données des plateformes et garantit une haute fiabilité des données aidant à évaluer les performances et utilisations des ENT et à en améliorer le pilotage.

### Domaine de confiance

Un **domaine de confiance** désigne l’ensemble des fournisseurs d’identité, des fournisseurs de services et des relations de confiance établies entre eux (pour leur permettre l’accès contrôlé et sécurisé aux différents services).

*« Un espace de confiance est un ensemble de ressources, de services informatiques et de services de communication qui permettent des échanges dans des conditions de sécurité suffisantes et cohérentes » (Journal Officiel, 27/12/2009). »*

### Donnée d’éducation

La donnée d’éducation est la donnée numérique personnelle « liée à la vie scolaire et concernant différents acteurs de la communauté éducative » (comité d’éthique pour les données d’éducation installé par le Ministre de l’Éducation nationale et de la Jeunesse en octobre 2019).

## E

### EcoIndex

Un service en ligne conçu et proposé par [GreenIT.fr](https://www.greenit.fr/), qui permet de prendre conscience de l’impact environnemental d’internet en estimant l’impact environnemental des pages web. La mesure est réalisée en prenant en compte le DOM, les requêtes HTTP et le poids des données transférées.

### ÉduConnect

Le service ÉduConnect est une solution d’authentification nationale des élèves et de leurs responsables qui leur attribue un compte unique pour les services numériques des écoles et des établissements. Il simplifie l’accès au suivi et à l’accompagnement de la scolarité des enfants et aux ressources numériques de l’éducation.

### ÉduGAR

Le service ÉduGAR permet aux élèves des 1<sup>er</sup> et 2<sup>d</sup> degrés d’accéder aux ressources via le GAR, pour les écoles et les établissements ne disposant pas d’ENT raccordé au GAR dans les territoires numériques éducatifs.

ÉduGAR est une solution mise à disposition par le ministère pour assurer le service d’accès aux ressources aux écoles et établissements non rattachés à un ENT territorial, via une authentification avec les guichets du ministère, un médiacentre et l’accès ressources du GAR. Le fonctionnement de la solution ÉduGAR est indépendant des projets ENT accrochés au GAR.

### ENT (Espace Numérique de Travail) / projet ENT / solution ENT

Un espace numérique de travail désigne un ensemble intégré de services numériques, organisé, choisi et mis à disposition de la communauté éducative par l’école ou l’établissement scolaire. Il repose sur un dispositif global fournissant à un usager un espace dédié à son activité dans le système éducatif. Il est un point d’entrée unifié pour accéder au système d’information pédagogique de l’école ou de l’établissement. Il offre un lieu d’échange et de collaboration entre ses usagers, et avec d’autres communautés en relation avec l’école ou l’établissement.

Source : [SDET](http://eduscol.education.fr/SDET)

**L’ENT** désigne cet ensemble de services en se situant du point de vue des usagers.

Le « **projet ENT** » désigne le projet d’ensemble (gouvernance, déploiement, accompagnement, évaluation, etc.) généralement porté en partenariat entre les collectivités territoriales, ministère chargé de l’Éducation nationale et les autorités en charge de l'enseignement agricole et maritime.

La « **solution ENT** » désigne les composants applicatifs et services de mise en œuvre proposés par les éditeurs, intégrateurs et autres prestataires (exploitants, hébergeurs) liés aux porteurs de projet par des engagements de service. Elle respecte l’architecture de référence ENT présentée dans le SDET.

## F

### Fournisseur d’identité

Le **fournisseur d’identité** se définit comme une composante de l’espace de confiance chargée de mettre à disposition un service d’identification pour les utilisateurs qui sont gérés dans son périmètre. Techniquement, le fournisseur d’identité assure l’authentification des utilisateurs et l’enrichissement du vecteur d’identification (par exemple : ajout d’attributs tels que la localisation ou le rôle fonctionnel de l’utilisateur).

### Fournisseur de service

Le **fournisseur de service** se définit comme une composante de l’espace de confiance mettant des services applicatifs et des ressources à disposition des utilisateurs et des organisations autorisées. Il est également chargé de gérer l’autorisation d’accès aux ressources et aux applications. Le fournisseur de service peut s’appuyer sur le fournisseur d’identité pour les fonctions d’identification et d’authentification.

### Fournisseur d’attributs

Un fournisseur d’identité peut être **fournisseur d’attributs** s’il envoie les données nécessaires au fournisseur de service pour la gestion des contrôles d’accès aux ressources ou la personnalisation des contenus.

## G

### GAR (Gestionnaire d’accès aux ressources)

La solution GAR est un traitement de données créé par le ministère chargé de l’Éducation nationale ayant pour objet de permettre l'accès des élèves et des enseignants à leurs ressources numériques et services associés via un espace numérique de travail (ENT), un navigateur internet (via ÉduGAR) ou une application native. Le GAR permet la communication des données strictement nécessaires aux distributeurs et éditeurs de ressources numériques pour l'éducation, désignés comme fournisseurs de ressources.

### Granule

Une granule est une unité didactique compacte et ciblée, conçue pour fournir un contenu éducatif essentiel de manière efficace et accessible.

## H

### Habilitations

Les habilitations des utilisateurs désignent les informations qui permettent à un service numérique le contrôle des autorisations pour l’accès (voir service d’autorisation/accès). Elles sont le résultat d’un paramétrage par un acteur désigné pour attribuer les droits d’accès en fonction des responsabilités en jeu.

### H5P

H5p est l’abréviation de HTML 5 Package. C’est un logiciel libre permettant de faciliter la création, le partage et la réutilisation de contenu interactif en fournissant une gamme de types de contenu pour divers besoins. Un contenu H5P peut être intégré dans n'importe quelle plate-forme prenant en charge un contexte de navigation imbriqué permettant d'obtenir une page HTML intégrée dans la page courante (iframes). H5P fournit notamment des intégrations pour les LMS via la norme LTI.

## J

### Jeu de données 

Groupe de données cohérent et structuré, portant sur un sujet déterminé.

## L

### LMS

Un LMS (Learning Management System) est un système logiciel de gestion de l'apprentissage. Il permet en général de créer et fournir des supports pédagogiques structurés, de les organiser, de suivre et gérer la progression des utilisateurs dans leurs apprentissages.

Un LMS comporte toujours deux interfaces : l’une administrateur, l’autre utilisateur.

### LTI Advantage

Le standard LTI[^33] (Learning Tools Interoperability, interopérabilité des outils d’apprentissage), émis par le consortium [1Edtech](https://www.1edtech.org/) (anciennement IMS Global), permet un échange sécurisé d'informations entre des outils d’apprentissage différents. LTI établit un moyen standard d'intégrer des applications d'apprentissage riches (souvent hébergées à distance et fournies via des services tiers) avec des plates-formes telles que des systèmes de gestion de l'apprentissage (learning management system - LMS), des portails ou d'autres environnements éducatifs. LTI propose plusieurs services et messages qui peuvent être utilisés pour faciliter les échanges d’information portant sur l’affectation des ressources et la notation, ou encore accéder à des granules par les liens profonds.

[^33]: <https://www.imsglobal.org/activity/learning-tools-interoperability>

## M

### Marqueur

Le marqueur est un élément de code qui, placé dans toutes les pages auditées, permet de recueillir les informations prédéfinies comme le nom de l’établissement, le profil de l’internaute, le type de page visitée, etc. et de transmettre ces données à un outil spécialisé dans le traitement des statistiques Internet.

Le marqueur est positionné en bas de page pour assurer qu’elle est bien vue lorsqu’elle est comptée.

### Médiacentre

Le Médiacentre est un espace qui regroupe les points d’accès vers les ressources pédagogiques éditoriales gratuites ou payantes, auxquelles l’usager a droit dans le cadre de l’école ou l’établissement. Le médiacentre assure donc une fonction de présentation. Il est fourni par l’[ENT](ENT) ou le fournisseur de service d’un projet numérique territorial, ou par le ministère (solution ÉduGAR pour des territoires qui ne disposent pas encore d’un projet ENT).

### MiNumEco

La MiNumEco est la mission interministérielle numérique écoresponsable pilotée par la DINUM. À travers elle, elle est engagée auprès des ministères pour permettre aux administrations de s’inscrire durablement dans les démarches pour un numérique responsable, notamment promouvoir un achat numérique responsable.

## O

### OMOGEN-API

Plateforme de gestion d'APIs (API Manager) qui permet de gérer la publication, la promotion et la supervision des API entre un service fournisseur et un service client, au sein d'un environnement sécurisé et évolutif.

### Opposabilité

Qualité de ce qui est opposable, l’opposabilité caractérise un acte juridique, qui a des effets à l’égard des tiers.

## P

### Portabilité des données

Possibilité de récupérer une partie des données produites par un utilisateur dans un format lisible par une machine.

### Profil

Un profil utilisateur est un ensemble d’informations concernant l'utilisateur, son (ses) rôle(s), ses préférences et le contexte dans lequel il se connecte qui peuvent être utiles pour la délivrance et le comportement du service.

### Pseudonymisation

La pseudonymisation est un traitement de données personnelles réalisé de manière à ce qu'on ne puisse plus attribuer les données relatives à une personne physique sans avoir recours à des informations supplémentaires. En pratique la pseudonymisation consiste à remplacer les données directement identifiantes (nom, prénom, etc.) d’un jeu de données par des données indirectement identifiantes (alias, numéro dans un classement, etc.).

La pseudonymisation permet ainsi de traiter les données d’individus sans pouvoir identifier ceux-ci de façon directe. En pratique, il est toutefois bien souvent possible de retrouver l’identité de ceux-ci grâce à des données tierces. C’est pourquoi des données pseudonymisées demeurent des données personnelles. L’opération de pseudonymisation est réversible, contrairement à l’anonymisation.

[Plus d’informations sur le site de la CNIL](https://www.cnil.fr/fr/lanonymisation-des-donnees-un-traitement-cle-pour-lopen-data).

## R

### RAAM

Le RAAM est le Référentiel d'évaluation de l'accessibilité des applications mobiles publié le 21 juin 2021 d’après la loi luxembourgeoise du 28 mai 2019. Le référentiel d’évaluation de l’accessibilité des applications mobiles a été élaboré pour servir de socle d’évaluation et d’implémentation pour tous les organismes du secteur public concernés par la loi. Ce référentiel propose un cadre opérationnel de vérification de la conformité aux exigences d’accessibilité. Dès lors, elle permet de vérifier qu’une application mobile est conforme aux critères décrits dans la norme européenne EN 301 549 v.3.2.1.

### REEN

La loi REEN du 5 novembre 2021 vise à réduire l'empreinte environnementale du numérique en France. Elle renforce la lutte contre l'obsolescence programmée, notamment logicielle, liée à l'installation de mises à jour inadaptées rendant un produit obsolète. 

### Ressources numériques éducatives

Les ressources numériques éducatives désignent tout contenu et outil au format numérique, au bénéfice de l’enseignement et de l’apprentissage. Elles s’adressent aux enseignants et aux élèves, pour un usage en classe et hors la classe. Elles doivent répondre aux orientations pédagogiques et aux prescriptions juridiques et techniques du MENJ.

### RGAA

Le [référentiel général d’amélioration de l’accessibilité (RGAA)](https://accessibilite.numerique.gouv.fr/ "référentiel général d’amélioration de l’accessibilité (RGAA)"), à forte dimension technique, offre une traduction opérationnelle des critères d’accessibilité issus des règles internationales ainsi qu'une méthodologie pour vérifier la conformité à ces critères.

La version 4.1 de ce référentiel a été publiée le 18 février 2021.

### RGESN

Les objectifs du [référentiel général d’écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/ "référentiel général d’écoconception de services numériques (RGESN)") sont de réduire la consommation de ressources informatiques et énergétiques et la contribution à l’obsolescence des équipements, qu’il s’agisse des équipements utilisateurs ou des équipements réseau ou serveur.

La version 1 de ce référentiel a été publiée le 28 novembre 2022.

### RGPD

Le [RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees "Lien vers le RGPD sur le site de la CNIL") est le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE (règlement général sur la protection des données).

Le règlement établit des règles relatives à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et des règles relatives à la libre circulation de ces données. Le règlement protège les libertés et droits fondamentaux des personnes physiques, et en particulier leur droit à la protection des données à caractère personnel. La libre circulation des données à caractère personnel au sein de l'Union n'est ni limitée ni interdite pour des motifs liés à la protection des personnes physiques à l'égard du traitement des données à caractère personnel.

### R2GA

Le [Référentiel général de gestion des archives (R2GA)](https://francearchives.gouv.fr/fr/circulaire/R2GA_2013_10 "Référentiel général de gestion des archives (R2GA)") est un ensemble de normes, de pratiques et de procédures définies pour garantir la gestion efficace et cohérente des archives au sein d'une organisation ou d'une entité administrative.

### Rôle

Un rôle est un regroupement de tâches et d'accréditations qui concourent à la réalisation d'une ou plusieurs fonctions. Il détermine un ensemble d'actions qui peuvent être effectuées par la personne ou le groupe auquel il est affecté. Une personne (ou un groupe) peut se voir affecter plusieurs rôles. Un rôle peut, ou non, constituer un élément de profil vis-à-vis d'un service donné.

## S

### Scope

Service outillant la circulation des données de l’organisation pédagogique de l’établissement (issues de l’emploi du temps annuel et opérationnel) dans un cadre maîtrisé et documenté, vers des acteurs habilités. Ce service est porté par le système d’information ministériel Siècle Vie de l’établissement.

### Service de découverte du fournisseur d’identité

Le **Service de découverte du fournisseur d’identité** ou WAYF (l’acronyme anglais pour pour « where are you from ») est une fonction d’aiguillage permettant de diriger l’utilisateur vers le fournisseur d’identité adéquat.

Ce service de découverte : 

* présente à l’utilisateur une liste de choix correspondant à des liens vers les fournisseurs d’identité ;
* lui demande de choisir en fonction des caractéristiques qui lui correspondent ;
* l’envoie vers la page de connexion du fournisseur d’identité choisi.

Une fonction de mémorisation du choix est souvent proposée (pour les utilisateurs qui utilisent systématiquement le même fournisseur d’identité).

### SNE – services numériques pour l'éducation

Les services numériques pour l'éducation sont des outils et ressources numériques mis à la disposition des enseignants, élèves et autres usagers de la communauté éducative dans le but immédiat d'enseigner, d'apprendre et de communiquer. Ils incluent des matériaux ou contenus d'apprentissage formatés numériquement et mis à la disposition des usagers, ou « ressources numériques éducatives ».

### SSO

SSO signifie « single sign on », soit « authentification unique ». C'est une méthode permettant à un utilisateur de ne procéder qu'à une seule authentification pour accéder à plusieurs applications informatiques ou sites web sécurisés (tant que l’authentification auprès du service d’authentification est valable).

Un service SSO est généralement capable de propager des informations d’identité dans l’objectif de contrôler l’accès à une ressource.

Les informations d’identité d’un utilisateur peuvent être ses identifiants, ses attributs ou encore les preuves de ses authentifications. La propagation de preuves d’authentification peut éviter à l’utilisateur de s’authentifier de nouveau.

## W

### WCAG

Le [Web content accessibility guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/ "Web content accessibility guidelines (WCAG)") ou en français les règles pour l’accessibilité des contenus du web définissent les lignes à suivre du [World Wide Web Consortium (W3C)](https://www.w3.org/ "World Wide Web Consortium (W3C)"). Ces dernières mettent en lumière les méthodes qui permettent de rendre les sites Internet accessibles à tous, en prenant en considération tous les aspects physiques, psychologiques et techniques des utilisateurs. En France on utilise le référentiel général d’amélioration de l’accessibilité (RGAA).

## X

### xAPI

xAPI, pour Experience API, est le standard le plus courant pour les systèmes d'apprentissage en ligne. Destinée à succéder à SCORM, xAPI est une spécification technique du format des traces d’apprentissage qui permet de suivre les parcours de formation même s’ils ont lieu dans divers environnements.

Cette norme de données et d'interface permet aux applications logicielles de capturer et de partager des données sur les performances des apprenants, ainsi que des informations contextuelles associées (c'est-à-dire des données « d'expérience »). xAPI peut être intégré à presque toutes les technologies d'apprentissage (nouvelles ou existantes), et il est indépendant du type de contenu d'apprentissage fourni. xAPI est une API open-source sous licence *Apache License*, Version 2.0.
