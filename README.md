# Doctrine technique du numérique pour l'éducation

Le ministère chargé de l’Éducation nationale publie la « doctrine technique du numérique pour l’éducation », afin de mettre en place un cadre d’architecture et de règles communes, visant à fournir aux usagers un ensemble lisible et structuré de services numériques éducatifs accessibles simplement et interopérables.

## Le corpus

La doctrine technique du numérique pour l'éducation est un corpus de cinq documents :

* le [document principal doctrine technique du numérique pour l’éducation](https://forge.apps.education.fr/doctrine-technique-numerique/doctrine-technique-numerique.forge.apps.education.fr) ; 
* le [cadre général de sécurité des services numériques pour l’éducation](https://forge.apps.education.fr/doctrine-technique-numerique/securite) ; 
* le [référentiel d’interopérabilité des services numériques pour l’éducation](https://forge.apps.education.fr/doctrine-technique-numerique/interoperabilite) ; 
* le [référentiel du numérique responsable pour l’éducation](https://forge.apps.education.fr/doctrine-technique-numerique/numerique-responsable) ; 
* un glossaire ; 

Chaque document a un projet sur la Forge des communs numériques qui lui est propre. La version en vigueur est [référencée sur Éduscol](https://eduscol.education.fr/3827/doctrine-technique-du-numerique-pour-l-education).

---

## La version en ligne de la doctrine technique du numérique pour l'éducation

Ce site utilise le thème [mkdocs-dsfr](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/mkdocs-dsfr), un thème mkdocs conforme aux standards de l'État français pour la création de sites Web.

- Voir cet exemple sur [la page Gitlab](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/numeco/mkdocs-dsfr-exemple)

### Prérequis

- [Python 3.x](https://www.python.org/downloads/)
- [pipenv](https://pipenv.pypa.io/en/latest/)

### Installation

1. Clonez ce dépôt sur votre machine locale.
2. Ouvrez un terminal et naviguez vers le dossier du projet.
3. Exécutez `pipenv install` pour installer les dépendances du projet.

###  Utilisation

#### Environnement de Développement

1. Entrez dans l'environnement virtuel avec `pipenv shell`.
2. Lancez le serveur de développement avec `mkdocs serve`.
3. Ouvrez votre navigateur Web et accédez à `http://127.0.0.1:8000`.

#### Construction du Site

Pour construire le site Web statique, utilisez la commande suivante :

```bash
mkdocs build
```

Le site sera généré dans le dossier `site`.

### Ressources Utiles

- [Site officiel de DSFR](https://www.systeme-de-design.gouv.fr/)
- [Documentation mkdocs](https://www.mkdocs.org/)